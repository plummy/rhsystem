require 'test_helper'

class PatientQueuesControllerTest < ActionController::TestCase
  setup do
    @patient_queue = patient_queues(:one)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:patient_queues)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create patient_queue' do
    assert_difference('PatientQueue.count') do
      post :create, patient_queue: {}
    end

    assert_redirected_to patient_queue_path(assigns(:patient_queue))
  end

  test 'should show patient_queue' do
    get :show, id: @patient_queue
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @patient_queue
    assert_response :success
  end

  test 'should update patient_queue' do
    patch :update, id: @patient_queue, patient_queue: {}
    assert_redirected_to patient_queue_path(assigns(:patient_queue))
  end

  test 'should destroy patient_queue' do
    assert_difference('PatientQueue.count', -1) do
      delete :destroy, id: @patient_queue
    end

    assert_redirected_to patient_queues_path
  end
end

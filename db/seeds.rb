# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# ruby encoding: utf-8

# Open connection
connection = ActiveRecord::Base.connection()

# Insert users default password: rizky123!
SmarterCSV.process ( "#{Rails.root.join('db', 'data')}/users.csv") do |chunk|
  chunk.each do |data_hash|
    User.create!(data_hash)
  end
end

SmarterCSV.process ( "#{Rails.root.join('db', 'data')}/locations.csv") do |chunk|
  chunk.each do |data_hash|
    Location.create!(data_hash)
  end
end

SmarterCSV.process ( "#{Rails.root.join('db', 'data')}/fields.csv") do |chunk|
  chunk.each do |data_hash|
    Field.create!(data_hash)
  end
end

SmarterCSV.process ( "#{Rails.root.join('db', 'data')}/provinces.csv") do |chunk|
  chunk.each do |data_hash|
    Province.create!(data_hash)
  end
end

SmarterCSV.process ( "#{Rails.root.join('db', 'data')}/regencies.csv") do |chunk|
  chunk.each do |data_hash|
    Regency.create!(data_hash)
  end
end

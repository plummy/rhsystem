class AddQueueNumberToPatientQueue < ActiveRecord::Migration
  def change
    add_column :patient_queues, :queue_number, :integer, default: 0
  end
end

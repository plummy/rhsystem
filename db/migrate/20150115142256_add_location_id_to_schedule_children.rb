class AddLocationIdToScheduleChildren < ActiveRecord::Migration
  def change
    add_column :schedule_children, :location_id, :int
  end
end

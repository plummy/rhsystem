class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string      :name
      t.string      :street
      t.belongs_to  :regency
      t.belongs_to  :province
      t.string      :country, default: 'ID'
      t.belongs_to  :doctor
      t.boolean     :is_active, default: true

      t.timestamps
    end
  end
end

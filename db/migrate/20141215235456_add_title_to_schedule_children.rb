class AddTitleToScheduleChildren < ActiveRecord::Migration
  def change
    add_column :schedule_children, :title, :string
  end
end

class RemoveScheduleInPatientQueue < ActiveRecord::Migration
  def change
    remove_column :patient_queues, :schedule_id, :int
  end
end

class AddRolesToUser < ActiveRecord::Migration
  def up
    add_column :users, :roles, :int, array: true, default: []
    remove_column :users, :role, :int
  end

  def down
    add_column :users, :role, :int
    remove_column :users, :roles, array: true, default: []
  end
end

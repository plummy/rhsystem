class AddIsAwayToSchedules < ActiveRecord::Migration
  def up
    add_column :schedules, :is_away, :boolean, default: false
  end

  def down
    remove_column :schedules, :is_away, :boolean, default: false
  end
end

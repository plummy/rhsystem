class CreatePatientQueues < ActiveRecord::Migration
  def change
    create_table :patient_queues do |t|
      t.belongs_to :patient
      t.belongs_to :schedule
      t.timestamps
    end
  end
end

class AddIdoctorToTmschedules < ActiveRecord::Migration
  def change
    add_column :schedules, :doctor_id, :integer
    add_column :schedules, :location_id, :integer
  end
end

class RemoveIsAwayInSchedule < ActiveRecord::Migration
  def change
    remove_column :schedules, :is_away, :boolean, default: false
  end
end

class AddDoctorToScheduleChildren < ActiveRecord::Migration
  def change
    add_column :schedule_children, :doctor_id, :integer
  end
end

class AddForeignKeyToPatientQueues < ActiveRecord::Migration
  def self.up
    add_foreign_key :patient_queues, :users, column: :patient_id, primary_key: 'id'
    add_foreign_key :patient_queues, :schedule_children, column: :schedule_child_id, primary_key: 'id'
  end

  def self.down
    remove_foreign_key :patient_queues, :users, column: :patient_id, primary_key: 'id'
    remove_foreign_key :patient_queues, :schedule_children, column: :schedule_child_id, primary_key: 'id'
  end
end

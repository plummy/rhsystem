class AddForeignKeyToSchedules < ActiveRecord::Migration
  def self.up
    add_foreign_key :schedules, :users, column: :doctor_id, primary_key: 'id'
    add_foreign_key :schedules, :locations, column: :location_id, primary_key: 'id'
  end

  def self.down
    remove_foreign_key :schedules, :users, column: :doctor_id, primary_key: 'id'
    remove_foreign_key :schedules, :locations, column: :location_id, primary_key: 'id'
  end
end

class AddLimitToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :limit, :integer, default: 0
  end
end

class AddOpenToScheduleChildren < ActiveRecord::Migration
  def change
    add_column :schedule_children, :open, :boolean, default: true
  end
end

class AddForeignKeyToScheduleChildren < ActiveRecord::Migration
  def self.up
    add_foreign_key :schedule_children, :schedules, column: :schedule_id, primary_key: 'id'
  end

  def self.down
    remove_foreign_key :schedule_children, :schedules, column: :schedule_id, primary_key: 'id'
  end
end

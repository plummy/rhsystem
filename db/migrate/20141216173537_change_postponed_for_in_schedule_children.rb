class ChangePostponedForInScheduleChildren < ActiveRecord::Migration
  def change
    change_column :schedule_children, :postponed_for, :int, default: 0
  end
end

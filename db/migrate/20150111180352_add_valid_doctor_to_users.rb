class AddValidDoctorToUsers < ActiveRecord::Migration
  def change
    add_column :users, :valid_doctor, :boolean, default: false
  end
end

class ChangeOpenColumn < ActiveRecord::Migration
  def change
    change_column :schedules, :open, :boolean, default: false
    change_column :schedule_children, :open, :boolean, default: true
  end
end

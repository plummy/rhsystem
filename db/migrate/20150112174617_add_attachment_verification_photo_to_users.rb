class AddAttachmentVerificationPhotoToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :verification_photo
    end
  end

  def self.down
    remove_attachment :users, :verification_photo
  end
end

class CreateScheduleChild < ActiveRecord::Migration
  def change
    create_table :schedule_children do |t|
      t.belongs_to :schedule
      t.datetime :start_time
      t.datetime :end_time
      t.integer :postponed_for
    end
  end
end

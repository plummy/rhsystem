class RefactorColumnsInSchedules < ActiveRecord::Migration
  def up
    remove_column :schedules, :days_id
    remove_column :schedules, :start_time
    remove_column :schedules, :end_time
    remove_column :schedules, :location_id
    remove_column :schedules, :repeat
    remove_column :schedules, :repeat_freq
    remove_column :schedules, :start_date
    remove_column :schedules, :title
    remove_column :schedules, :limit
    remove_column :schedules, :open
    remove_column :schedules, :note
  end

  def down
    add_column :schedules, :days_id, :integer
    add_column :schedules, :start_time, :time
    add_column :schedules, :end_time, :time
    add_column :schedules, :location_id, :integer
    add_column :schedules, :repeat, :integer
    add_column :schedules, :repeat_freq, :integer
    add_column :schedules, :start_date, :datetime
    add_column :schedules, :title, :string
    add_column :schedules, :limit, :integer
    add_column :schedules, :open, :boolean
    add_column :schedules, :note, :text
  end
end

class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.boolean :is_active, default: true
      t.integer :role
      t.integer :field_id

      t.timestamps
    end
  end
end

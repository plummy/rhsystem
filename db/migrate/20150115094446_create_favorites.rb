class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.belongs_to :patient
      t.belongs_to :doctor
    end
  end
end

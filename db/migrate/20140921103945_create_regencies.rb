class CreateRegencies < ActiveRecord::Migration
  def change
    create_table :regencies do |t|
      t.belongs_to :province
      t.timestamps
    end
end
end

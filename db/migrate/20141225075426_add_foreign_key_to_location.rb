class AddForeignKeyToLocation < ActiveRecord::Migration
  def self.up
    add_foreign_key :locations, :users, column: :doctor_id, primary_key: 'id'
  end

  def self.down
    remove_foreign_key :locations, :users, column: :doctor_id, primary_key: 'id'
  end
end

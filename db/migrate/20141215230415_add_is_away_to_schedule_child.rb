class AddIsAwayToScheduleChild < ActiveRecord::Migration
  def change
    add_column :schedule_children, :is_away, :boolean, default: false
  end
end

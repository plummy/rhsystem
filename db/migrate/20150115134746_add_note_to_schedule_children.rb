class AddNoteToScheduleChildren < ActiveRecord::Migration
  def change
    add_column :schedule_children, :note, :text
  end
end

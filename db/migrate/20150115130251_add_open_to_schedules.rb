class AddOpenToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :open, :boolean, default: true
  end
end

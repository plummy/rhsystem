class ChangeOpenColumnToAllFalse < ActiveRecord::Migration
  def change
    change_column :schedules, :open, :boolean, default: false
    change_column :schedule_children, :open, :boolean, default: false
  end
end

class AddLimitToScheduleChildren < ActiveRecord::Migration
  def change
    add_column :schedule_children, :limit, :integer, default: 0
  end
end

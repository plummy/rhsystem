class AddScheduleChildToPatientQueue < ActiveRecord::Migration
  def change
    add_reference :patient_queues, :schedule_child, index: true
  end
end

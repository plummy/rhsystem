class AddTitleToUsers < ActiveRecord::Migration
  def up
    add_column :users, :title, :string, limit: 20
  end

  def down
    add_column :users, :title, :string, limit: 20
  end
end

class AddRepeatsAndRepeatFreqAndStartDateToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :repeat, :integer, limit: 1
    add_column :schedules, :repeat_freq, :integer, limit: 1
    add_column :schedules, :start_date, :timestamp
  end
end

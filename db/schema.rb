# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_150_131_150_932) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'favorites', force: :cascade do |t|
    t.integer 'patient_id'
    t.integer 'doctor_id'
  end

  create_table 'fields', force: :cascade do |t|
    t.string   'name'
    t.boolean  'is_active', default: true
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'code'
  end

  create_table 'locations', force: :cascade do |t|
    t.string   'name'
    t.string   'street'
    t.integer  'regency_id'
    t.integer  'province_id'
    t.string   'country', default: 'ID'
    t.integer  'doctor_id'
    t.boolean  'is_active', default: true
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.float    'latitude'
    t.float    'longitude'
    t.string   'phone'
  end

  create_table 'patient_queues', force: :cascade do |t|
    t.integer  'patient_id'
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.integer  'schedule_child_id'
    t.integer  'queue_number', default: 0
  end

  add_index 'patient_queues', ['schedule_child_id'], name: 'index_patient_queues_on_schedule_child_id', using: :btree

  create_table 'provinces', force: :cascade do |t|
    t.string   'name'
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  create_table 'regencies', force: :cascade do |t|
    t.integer  'province_id'
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'name'
  end

  create_table 'schedule_children', force: :cascade do |t|
    t.integer  'schedule_id'
    t.datetime 'start_time'
    t.datetime 'end_time'
    t.integer  'postponed_for', default: 0
    t.boolean  'is_away',       default: false
    t.string   'title'
    t.integer  'limit',         default: 0
    t.boolean  'open',          default: false
    t.text     'note'
    t.integer  'location_id'
    t.integer  'doctor_id'
  end

  create_table 'schedules', force: :cascade do |t|
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.integer  'doctor_id'
  end

  create_table 'users', force: :cascade do |t|
    t.string   'name'
    t.string   'address'
    t.string   'phone'
    t.boolean  'is_active', default: true
    t.integer  'field_id'
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'email',                                      default: '',    null: false
    t.string   'encrypted_password',                         default: '',    null: false
    t.string   'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer  'sign_in_count', default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.inet     'current_sign_in_ip'
    t.inet     'last_sign_in_ip'
    t.integer  'roles', default: [], array: true
    t.text     'description'
    t.string   'avatar_file_name'
    t.string   'avatar_content_type'
    t.integer  'avatar_file_size'
    t.datetime 'avatar_updated_at'
    t.boolean  'avatar_processing'
    t.string   'title', limit: 20
    t.boolean  'valid_doctor', default: false
    t.string   'verification_photo_file_name'
    t.string   'verification_photo_content_type'
    t.integer  'verification_photo_file_size'
    t.datetime 'verification_photo_updated_at'
  end

  add_index 'users', ['email'], name: 'index_users_on_email', unique: true, using: :btree
  add_index 'users', ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true, using: :btree

  add_foreign_key 'locations', 'users', column: 'doctor_id'
  add_foreign_key 'patient_queues', 'schedule_children'
  add_foreign_key 'patient_queues', 'users', column: 'patient_id'
  add_foreign_key 'schedule_children', 'schedules'
  add_foreign_key 'schedules', 'users', column: 'doctor_id'
end

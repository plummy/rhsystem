if Rails.env.production?
  ENV['ELASTICSEARCH_URL'] = 'http://' + ENV['SEARCH_1_PORT_9200_TCP_ADDR'].to_s + ':' + ENV['SEARCH_1_PORT_9200_TCP_PORT'].to_s
else
  ENV['ELASTICSEARCH_URL'] = 'http://' + ENV['SEARCH_1_PORT_9200_TCP_ADDR'].to_s + ':' + ENV['SEARCH_1_PORT_9200_TCP_PORT'].to_s
end

require 'resque/server'

Rhsystem::Application.routes.draw do
  root to: 'static_pages#home'

  mount Resque::Server.new, at: '/resque'

  get '/home', to: 'static_pages#home'
  get '/help', to: 'static_pages#help'

  match 'schedules/change_schedule' => 'schedules#change_schedule', as: :change_schedule, via: :get

  match 'schedules/set_away/:id(.:format)' => 'schedules#set_away_status', as: :set_schedule_status, via: :get
  match 'schedules/set_open/:id(.:format)' => 'schedules#set_open_status', as: :set_schedule_open, via: :get
  match 'locations/get_city/:id(.:format)' => 'locations#get_city', as: :get_city, via: :get
  match 'schedules/table_preview' => 'schedules#table_preview', as: :get_schedule, via: :get

  match 'schedule_children/destroy/:id(.:format)' => 'schedule_children#destroy', as: :delete_event, via: :delete
  match 'schedule_children/destroy_one/:id(.:format)' => 'schedule_children#destroy_one', as: :delete_one_event, via: :delete

  get '/schedule_children/show_queue/:id(.:format)' => 'schedule_children#show_queue', as: :show_queue
  get '/schedules/fetch_calendar' => 'schedules#fetch_calendar', as: :fetch_calendar
  get '/schedules/fetch_pagination' => 'schedules#fetch_pagination', as: :fetch_pagination
  get '/schedules/fetch_stats' => 'schedules#fetch_stats', as: :fetch_stats
  get '/schedule_children/is_recurring' => 'schedule_children#is_recurring_event', as: :is_recurring

  concern :search_doctor do
    scope '/search', as: :search do
      get '/', to: 'users#search'
      get '/schedule/:id', to: 'schedule_children#doctor', as: :schedule
    end
  end

  # concerns :search_doctor

  devise_for :users, controllers: { registrations:  'registrations' }

  devise_scope :user do
    get 'signin', to: 'devise/sessions#new'
    delete 'signout', to: 'devise/sessions#destroy'
    get 'signup', to: 'devise/registrations#new'
    get 'reset', to: 'devise/passwords#new'
    get '/switch', to: 'users#switch'
    get '/queue/:id(.:format)', to: 'patient_queues#index', as: :queue
    delete '/queue', to: 'patient_queues#destroy'

    concerns :search_doctor
    scope :dashboard, as: :dashboard do
      get '/profile', to: 'users#profile'
      put '/profile/update_field', to: 'registrations#update_field'
      get '/settings', to: 'users#settings', as: :settings
      scope :settings, as: :settings do
        get '/edit(.:format)', to: 'registrations#edit', format: :json, as: :password
        put '/update(.:format)', to: 'registrations#update', as: :password_update
        get '/verifications', to: 'users#welcome'
        get '/verifications/location', to: 'locations#verification_locations'
        get '/verifications/stepone', to: 'users#verification_doctor'
      end

      # Doctor's menu
      get '/doctor', to: 'users#doctor_dashboard'
      get '/schedules', to: 'schedules#index'
      get '/schedules/new', to: 'schedules#new'
      post '/schedules/create', to: 'schedules#create'
      get '/event/:id(.:format)' => 'schedule_children#edit'
      match 'event/update/:id(.:format)' => 'schedule_children#update', via: :patch
      get '/schedule/:id', to: 'schedules#doctors_schedule', as: :schedule_data
      resources :locations, except: :show

      # Patient's menu
      get '/patient', to: 'users#patient_dashboard'
      scope :favorites, as: :favorites do
        get '/', to: 'favorites#index'
        post '/', to: 'favorites#create'
      end
      concerns :search_doctor
      scope :appointments, as: :appointments do
        get '/', to: 'patient_queues#appointments'
        post '/', to: 'patient_queues#create'
        get '/:id', to: 'patient_queues#show', as: :detail
      end
    end
  end
end

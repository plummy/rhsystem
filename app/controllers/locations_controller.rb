class LocationsController < ApplicationController
  before_action :set_location, only: [:edit, :update, :destroy]

  # GET /locations
  # GET /locations.json
  def index
    @locations = Location.where(doctor_id: current_user.id).order(id: :asc)
  end

  # GET /locations/new
  def new
    @location = Location.new

    # assign this one so that simple_form_for will know
    # that it is going to be create action
    @doctor = current_user
  end

  def verification_locations
    @location = Location.new
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  # POST /locations.json
  def create
    @location = Location.new(location_params)
    @location.doctor_id = current_user.id

    # assign this one so that simple_form_for will know
    # that it is going to be create action
    @doctor = current_user

    respond_to do |format|
      if @location.save
        if params[:location][:origin] == 'settings'
          format.html { redirect_to dashboard_settings_verifications_path, notice: 'Location was successfully created.' }
        else
          format.html { redirect_to dashboard_locations_path, notice: 'Location was successfully created.' }
        end
      else
        format.html { render action: 'new' }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to dashboard_locations_path, notice: 'Location was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_locations_path }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_location
    @location = Location.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def location_params
    params.require(:location).permit(:name, :street, :regency_id, :province_id, :phone)
  end
end

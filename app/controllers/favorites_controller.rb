class FavoritesController < ApplicationController
  before_action :set_favorite, only: [:destroy]
  before_action :authenticate_user!

  # GET /favorites
  # GET /favorites.json
  def index
    @favorites = Favorite.where(patient_id: current_user.id).order(id: :asc)
  end

  # POST /favorites
  # POST /favorites.json
  def create
    @favorite = Favorite.new(favorite_params)
    @favorite.patient_id = current_user.id

    respond_to do |format|
      if @favorite.save
        format.js
      else
        format.js { render 'error_create' }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_favorite
    @favorite = Favorite.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def favorite_params
    params.require(:favorite).permit(:doctor_id)
  end
end

class ApplicationController < ActionController::Base
  helper_method :set_active_role, :active_role?, :is_dashboard?, :active_role
  respond_to :json, :html
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  # The lazy way configuring additional fields on devise
  # Read here: http://devise.plataformatec.com.br/#getting-started/strong-parameters
  # for further information
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :set_layout

  def configure_permitted_parameters
    if resource_class == User
      devise_parameter_sanitizer.for(:sign_up) do |u|
        u.permit(
          :name,
          :address,
          :phone,
          :email,
          :password,
          :password_confirmation
        )
      end
    end
  end

  # Override error message from devise
  def errors_for(model, attribute)
    if model.errors[attribute].present?
      content_tag :span, class: 'error_explanation' do
        model.errors[attribute].join(', ')
      end
    end
  end

  # To make sure that the browsers wouldn’t cache the page
  # when someone clicked off and then hit the back button.
  # Thanks to:
  # http://blog.serendeputy.com/posts/how-to-prevent-browsers-from-caching-a-page-in-rails/

  before_action :set_cache_buster

  def set_cache_buster
    response.headers['Cache-Control'] = 'no-cache, no-store, max-age=0, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = 'Fri, 01 Jan 1990 00:00:00 GMT'
  end

  # Redirect to a page after Sign in success
  def after_sign_in_path_for(_resource_or_scope)
    if current_user.is_only_patient?
      dashboard_patient_path
    else
      switch_path
    end
  end

  def after_sign_up_path_for(_resource_or_scope)
    patient_dashboard_path
  end

  # Redirect to a page after Sign out success
  def after_sign_out_path_for(_resource_or_scope)
    root_path
  end

  # execute a block with a different format (ex: an html partial while in an ajax request)
  def with_format(format, &_block)
    old_formats = formats
    self.formats = [format]
    yield
    self.formats = old_formats
    nil
  end

  def get_remote_ip
    request.remote_ip == '127.0.0.1' ? '10.221.147.207' : request.remote_ip
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    redirect_to root_url
  end

  ## set active role by calling set_active_role :role
  def set_active_role(role)
    session[:active_role] = role
  end

  ## get active role now
  def active_role
    session[:active_role]
  end

  ## check active role by calling active_role? :role
  def active_role?(role)
    active_role == role
  end

  def is_dashboard?
    request.path.split('/').second == 'dashboard' || request.path.split('/').second == 'users'
  end

  protect_from_forgery with: :exception

  private

  def set_layout
    'dashboard' if is_dashboard? && user_signed_in?
  end
end

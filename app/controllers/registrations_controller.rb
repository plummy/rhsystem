class RegistrationsController < Devise::RegistrationsController
  before_action :set_user, only: [:update, :update_field]

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  def update_field
    unless params.key? :doctor
      if active_role? :doctor
        updated_params = ActionController::Parameters.new(doctor: {
                                                            params['name'].to_sym => params['value']
                                                          })

        update_profile(@user, updated_params, :doctor)
      else
        updated_params = ActionController::Parameters.new(user: {
                                                            params['name'].to_sym => params['value']
                                                          })
        update_profile(@user, updated_params, :user)
      end
    else
      upgrade_doctor
      respond_to do |format|
        if @doctor.save!
          format.html { redirect_to dashboard_settings_verifications_location_path, notice: 'User was successfully updated.' }
        else
          format.html { render action: 'verification_doctor', controller: 'users' }
          format.json { render json: @doctor.errors, status: :unprocessable_entity }
        end
      end

    end
  end

  def update_profile(user, params, role)
    respond_to do |format|
      if needs_password?(role, params)
        if user.update_with_password(account_update_params)
          set_user
          format.json { head :no_content, status: :ok }
          format.js { render layout: false }
        else
          format.js { render layout: false }
          format.json { render json: user.errors, status: :unprocessable_entity }
        end
      else
        params[role].delete(:current_password)
        permitted = params.require(role).permit(:name, :address, :phone, :description)
        if user.update_without_password(permitted)
          set_user
          format.json { head :no_content, status: :ok }
          format.js { render layout: false }
        else
          format.js { render layout: false }
          format.json { render json: user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private

  # check if we need password to update user data
  # ie if password or email was changed
  # extend this as needed
  def needs_password?(user, params)
    params[user][:password].present?
  end

  def user_params
    params.require(:doctor).permit(:name, :address, :phone)
  end

  def set_user
    @user = User.find(current_user.id)
    @doctor = Doctor.find(current_user.id) if current_user.is? :doctor
  end

  def upgrade_doctor
    @doctor = User.where(id: current_user.id).first_or_initialize
    @doctor = @doctor.becomes(Doctor)

    ## Please remove upgrade_to_doctor and valid_doctor if someday you need to validate doctor
    @doctor.upgrade_to_doctor
    @doctor.valid_doctor = true

    @doctor.field_id = params[:doctor][:field_id]
    @doctor.verification_photo = params[:doctor][:verification_photo]
    @doctor.title = params[:doctor][:title]
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :email, :password, :password_confirmation, :address, :phone)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:name, :phone, :password, :password_confirmation, :current_password, :field_id, :description, :address)
    end
  end
end

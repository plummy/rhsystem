class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:set_away_status, :change_schedule, :set_open_status]

  def index
    # Store it in a session
    @doctor = Doctor.find(current_user.id)
    @date = Date.current
  end

  # return json to calendar
  def fetch_calendar
    @calendar = Doctor.find(current_user.id).schedule_children.based_on_location(session[:location_id]).is_between(params[:start], params[:end])
  end

  # return js to paginator
  def fetch_pagination
    @schedules = Doctor.find(current_user.id).schedule_children.based_on_location(session[:location_id]).is_between(Time.now, Time.now + 7.days).order(start_time: :asc).page(params[:page]).per(7)
    respond_to do |format|
      format.js { render layout: false }
    end
  end

  # return json to stats
  def fetch_stats
    render json: Doctor.find(current_user.id).patient_queues.based_on_location(session[:location_id]).group_by_day('patient_queues.created_at', format: '%-d-%b-%Y').count
  end

  # GET /schedules/new
  def new
    @schedule = Schedule.new
  end

  # This will create an master event followed by event_child in the background
  def create
    @schedule = Schedule.new(schedule_params)
    @schedule.doctor_id = current_user.id

    respond_to do |format|
      if @schedule.save
        ChildEventJob.perform_later @schedule
        refresh_schedule
        format.html { redirect_to dashboard_schedules_path, notice: 'Success. Please wait until we generated your calendar' }
      else
        format.html { render action: 'new' }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  ## Change away status of schedule_children table
  def set_away_status
    @schedule_child.is_away = @schedule_child.is_away ? false : true
    respond_to do |format|
      if @schedule_child.save
        @doctor = Doctor.find(current_user.id)
        @date = @schedule_child.start_time.to_date
        # format.json { render json: @schedule_child.is_away, status: :ok }
        format.js { render layout: false }
      end
    end
  end

  def set_open_status
    @schedule_child.open = @schedule_child.open ? false : true
    respond_to do |format|
      if @schedule_child.save
        @doctor = Doctor.find(current_user.id)
        @date = @schedule_child.start_time.to_date
        # format.json { render json: @schedule_child.is_away, status: :ok }
        format.js { render layout: false }
      end
    end
  end

  ## Change schedule is for change time and date by calendar view of schedule_children table
  def change_schedule
    @schedule_child.end_time = params[:end_time]
    @schedule_child.start_time = params[:start_time]

    ## After changing schedule set postponed_for to :never
    ## So it will not crashed with postpone feature
    @schedule_child.postponed_for = :never

    respond_to do |format|
      if @schedule_child.save
        format.js { render layout: false }
      else
        format.js { render layout: false }
        format.json { render json: schedule_child.errors, status: :unprocessable_entity }
      end
      refresh_schedule
    end
  end

  def doctors_schedule
    @doctor = Doctor.find(params[:id])
    @date = params[:date] ? params[:date] : Date.current

    respond_to do |format|
      format.html { render 'schedules/index' }
      format.json { render 'schedules/doctor/events' }
      format.js { render 'schedules/shared/schedules' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_schedule
    @schedule_child = ScheduleChild.find(params[:id])
  end

  # Refreshing schedule table
  def refresh_schedule
    @schedules = Doctor.find(current_user.id).schedule_children.based_on_location(session[:location_id]).is_between(Time.now, Time.now + 7.days).order(start_time: :asc).page(params[:page]).per(7)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def schedule_params
    params.require(:schedule).permit(:start_time, :end_time, :location_id, :repeat_freq, :title, :start_date)
  end

  def direct_to_schedules
    redirect_to schedules_path(location_id: params[:location_id])
  end
end

class StaticPagesController < ApplicationController
  def home
    unless current_user.nil?
      unless active_role.nil?
        redirect_to dashboard_profile_path
      else
        redirect_to switch_path
      end
    end
  end

  def help
  end
end

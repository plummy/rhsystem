class ScheduleChildrenController < ApplicationController
  before_action :set_schedule_child, only: [:edit, :update, :destroy, :destroy_one, :update_schedule_child_and_schedule]

  def edit
    # this has been implemented in set_schedule_child
  end

  def update
    respond_to do |format|
      if params[:update_single_schedule]
        if update_schedule_child_and_schedule
          format.html { redirect_to dashboard_schedules_path, notice: 'Schedule was successfully updated.' }
        else
          format.html { render action: 'edit' }
          format.json { render json: @schedule_child.errors, status: :unprocessable_entity }
        end
      elsif params[:update_all_schedule]
        if ScheduleChild.update_all_schedule_children(schedule_child_params, @schedule_child.schedule_id)
          format.html { redirect_to dashboard_schedules_path, notice: 'Schedule was successfully updated.' }
        else
          format.html { render action: 'edit' }
          format.json { render json: @schedule_child.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def show_queue
    @patient = PatientQueue.joins([schedule_child: { schedule: :doctor }], :patient)
                           .based_on_doctor_and_location(current_doctor.id, session[:location_id])
                           .based_on_schedule_child(params[:id])
                           .select('schedule_children.id as id, patients.id as patient_id, patients.name, patients.phone')
  end

  def destroy
    @schedule = Schedule.find(@schedule_child.schedule_id)
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to location_schedules_path(session[:location_id]) }
      format.json { head :no_content }
    end
  end

  def destroy_one
    @schedule_child.destroy
    respond_to do |format|
      format.html { redirect_to location_schedules_path(session[:location_id]) }
      format.json { head :no_content }
    end
  end

  def is_recurring_event
    @schedule = Schedule.find(@schedule_child.schedule_id)
    is_recurring = @schedule.repeat == 'single_event' ? true : false
    respond_to do |format|
      format.json { render json: is_recurring, status: :ok }
    end
  end

  # Shows doctor's schedules
  def doctor
    @doctor = Doctor.find(params[:id])
    date = params[:date] ? params[:date] : Date.current
    @schedule_collection = @doctor.schedule_children.valid_schedules_for_patient(date, current_user.id)

    respond_to do |format|
      format.html { render 'schedule_children/doctor/index' }
      format.json { render 'schedule_children/doctor/events' }
      format.js { render 'schedule_children/doctor/schedules' }
    end
  end

  private

  def set_schedule_child
    @schedule_child = ScheduleChild.find(params[:id])
  end

  def update_schedule_child_and_schedule
    @schedule_child.transaction do
      @schedule = Schedule.new(start_time: schedule_child_params[:start_time],
                               end_time: schedule_child_params[:end_time],
                               location_id: schedule_child_params[:location_id],
                               repeat_freq: :never,
                               title: schedule_child_params[:title],
                               start_date: schedule_child_params[:start_time])
      schedule_child_params[:schedule_id] = @schedule_id
      @schedule_child.update(schedule_child_params)
    end
  end

  def schedule_child_params
    params.require(:schedule_child).permit(:start_time,
                                           :end_time,
                                           :title,
                                           :postponed_for,
                                           :update_single_schedule,
                                           :update_all_schedule,
                                           :location_id,
                                           :limit,
                                           :note,
                                           :schedule_id)
  end
end

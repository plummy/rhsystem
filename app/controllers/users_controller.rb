class UsersController < Devise::RegistrationsController
  before_action :set_user, only: [:switch, :profile, :update, :settings, :upload_verification]
  before_action :update_doctor, only: [:verification_doctor]
  before_action :authenticate_user!, except: [:search]

  def switch
    @roles = current_user.roles
  end

  def doctor_dashboard
    session[:active_role] = :doctor
    redirect_to dashboard_profile_path
  end

  def patient_dashboard
    session[:active_role] = :patient
    redirect_to dashboard_profile_path
  end

  def settings
    redirect_to dashboard_settings_password_path
  end

  def welcome
    render 'users/verifications/welcome'
  end

  def verification_doctor
    render 'users/verifications/verification_doctor'
  end

  def upload_verification
  end

  # def get_fields
  #   render json: Field.select('id as value', 'name as text')
  # end

  def update
    respond_to do |format|
      if active_role? :doctor
        if @doctor.update(doctor_params)
          format.json { head :no_content }
        else
          format.json { head :no_content }
        end
      end
    end
  end

  def search
    @doctors = []
    if params[:keyword].present?
      @doctors = Doctor.search params[:keyword],
                               misspellings: { edit_distance: 3 },
                               page: params[:page],
                               per_page: 10,
                               limit: 100,
                               fields: [{ name: :word }, { name: :word_start }, { name: :word_middle }, { name: :word_end }, :code, :field_name]
    end

    respond_to do |format|
      format.html { render 'users/search/index' }
      format.js { render 'users/search/results' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
    @doctor = Doctor.find(current_user.id) if active_role? :doctor
  end

  def update_doctor
    @user = current_user
    @doctor = Doctor.new @user.attributes
  end

  def doctor_params
    # @json_params = ActionController::Parameters.new( JSON.parse(request.body.read) )
    params.require(:doctor).permit(:name, :address, :phone, :field_id, :description)
  end

  def user_params
    params.require(:user).permit(:name, :address, :phone)
  end
end

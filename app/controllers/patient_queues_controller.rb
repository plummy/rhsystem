class PatientQueuesController < ApplicationController
  load_and_authorize_resource
  # before_action :set_patient_queue, only: [:show, :edit, :update, :destroy]
  before_action :set_patient_queue, only: [:show]

  # GET /patient_queues
  # GET /patient_queues.json
  def index
    @patient_queues = Doctor.find(current_user.id).schedule_children.find(params[:id]).patient_queues
    @date = ScheduleChild.find(params[:id]).start_time
  end

  # GET /patient_queues/1
  # GET /patient_queues/1.json
  def show
  end

  # GET /patient_queues/new
  def new
    @patient_queue = PatientQueue.new
  end

  # GET /patient_queues/1/edit
  def edit
  end

  # POST /patient_queues
  # POST /patient_queues.json
  def create
    @patient_queue = PatientQueue.new(patient_queue_params)

    respond_to do |format|
      if @patient_queue.save
        format.html { redirect_to @patient_queue, notice: 'Patient queue was successfully created.' }
        format.js { render 'patient_queues/appointments/create' }
        format.json { render action: 'show', status: :created, location: @patient_queue }
      else
        format.html { render action: 'new' }
        format.json { render json: @patient_queue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patient_queues/1
  # PATCH/PUT /patient_queues/1.json
  def update
    respond_to do |format|
      if @patient_queue.update(patient_queue_params)
        format.html { redirect_to @patient_queue, notice: 'Patient queue was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @patient_queue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patient_queues/1
  # DELETE /patient_queues/1.json
  def destroy
    @patient_queue.destroy
    respond_to do |format|
      format.html { redirect_to patient_queues_url }
      format.json { head :no_content }
    end
  end

  def appointments
    @appointments = PatientQueue.where(patient_id: current_user.id)
                                .order(created_at: :desc)
                                .page(params[:page])
                                .per(10)
    render 'patient_queues/appointments/index'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_patient_queue
    @patient_queue = PatientQueue.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def patient_queue_params
    params[:patient_queue] = {
      schedule_child_id: params[:schedule_child_id],
      patient_id: current_user.id
    }
  end
end

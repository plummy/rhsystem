class Field < ActiveRecord::Base
  has_many :doctors
  def to_value_and_text
    { value: id, text: name }
  end
end

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    alias_action :create, :read, :update, :destroy, to: :crud

    if user.is? :patient
      can :crud, [Patient, PatientQueue]
      can :read, [ScheduleChild]
      can :appointments, PatientQueue
    end
    can :crud, [Location, Schedule, ScheduleChild] if user.is? :doctor
  end
end

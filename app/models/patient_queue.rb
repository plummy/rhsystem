class PatientQueue < ActiveRecord::Base
  # Validations
  validates :patient, :schedule_child, presence: true
  validate :is_not_exist

  # Relations
  belongs_to :schedule_child
  belongs_to :patient

  before_create :insert_queue_number

  scope :based_on_doctor_and_location, -> (doctor_id, location_id) { where('schedules.doctor_id = ? AND schedules.location_id = ?', doctor_id, location_id) }
  scope :based_on_location, -> (location_id) { where('schedules.location_id = ?', location_id) }
  scope :based_on_schedule_child, -> (schedule_child_id) { where('schedule_children.id = ?', schedule_child_id) }
  scope :based_on_doctor, -> (doctor_id) { where('schedules.doctor_id = ?', doctor_id) }

  def is_exist?
    PatientQueue.find_by_schedule_child_id_and_patient_id(schedule_child_id, patient_id).present?
  end

  private

  def insert_queue_number
    last_id = PatientQueue.where('schedule_child_id = ?', schedule_child_id).count
    self.queue_number = last_id + 1
  end

  def is_not_exist
    errors.add(:schedule_child, :is_exist) if is_exist?
  end
end

class Schedule < ActiveRecord::Base
  # attributes not mapped to database
  attr_accessor :start_time, :end_time, :location_id, :doctor_id, :title, :limit, :note, :days_id, :repeat, :repeat_freq, :start_date

  # Validations
  validates :start_time, presence: true
  validates :end_time, presence: true
  validate  :is_end_time_larger_than_start_time
  before_validation :is_recurring_event, :set_days_id

  # Relations
  belongs_to :doctor
  has_many :schedule_children, dependent: :destroy

  # Enum
  enum repeat: {
    single_event: 0,
    recurring_event: 1 }

  enum repeat_freq: {
    never: 0,
    daily: 1,
    weekly: 7,
    monthly: 30 }

  enum days_id: {
    sunday: 1,
    monday: 2,
    tuesday: 3,
    wednesday: 4,
    thursday: 5,
    friday: 6,
    saturday: 7
  }

  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end

  def self.time_zone_aware_attributes
    false
  end

  def is_end_time_larger_than_start_time
    if end_time_is_larger_than_start_time?
      errors.add(:end_time, 'must be larger than start time')
    end
  end

  def set_days_id
    self.days_id = start_date.strftime('%A').downcase.to_sym unless start_date.nil?
  end

  def end_time_is_larger_than_start_time?
    result = end_time.nil? && start_time.nil? ? 0 : end_time - start_time
    result < 0 ? true : false
  end

  def is_recurring_event
    self.repeat = repeat_freq != 'never' ? :recurring_event : :single_event
  end
end

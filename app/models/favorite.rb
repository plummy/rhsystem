class Favorite < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :patient

  validates :doctor, :patient, presence: true
  validate :doctor_is_not_current_user, :is_not_exist

  def it_self?
    doctor_id == patient_id
  end

  def is_exist?
    Favorite.find_by_doctor_id_and_patient_id(doctor_id, patient_id).present?
  end

  private

  def doctor_is_not_current_user
    errors.add(:doctor, :doctor_is_current_user) if it_self?
  end

  def is_not_exist
    errors.add(:doctor, :is_exist) if is_exist?
  end
end

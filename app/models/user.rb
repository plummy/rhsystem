class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  before_create :set_default_role

  has_attached_file :avatar, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: '/images/:style/missing.png'
  process_in_background :avatar, url_with_processing: false

  # Validations
  validates :name, presence: true
  validates :address, presence: true
  validates :phone, presence: true
  validates :email, presence: true
  validates :password, presence: true, on: :create
  validates :password, length: { minimum: 5, maximum: 120 }, on: :update, allow_blank: true
  validates_attachment_file_name :avatar, matches: [/png\Z/, /jpe?g\Z/]

  enum role_types: [:doctor, :patient]

  ## Usage:
  ## u = User.last
  ## u.is? :doctor
  def is?(role)
    roles.include? User.role_types[role]
  end

  def valid_doctor?
    valid_doctor == true
  end

  def set_default_role
    set_role :patient unless roles.include? User.role_types[:patient]
  end

  def is_only_patient?
    is?(:patient) && roles.count == 1
  end

  def set_role(role)
    roles << User.role_types[role]
  end
end

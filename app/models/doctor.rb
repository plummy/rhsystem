class Doctor < User
  # For better searching
  searchkick word: [:name, :code, :field_name], word_start: [:name], word_middle: [:name], word_end: [:name]
  has_attached_file :verification_photo, styles: { thumb: '100x100>' }
  process_in_background :verification_photo, url_with_processing: false

  has_many :favorites
  has_many :locations
  has_many :schedule_children, dependent: :destroy
  has_many :patient_queues, through: :schedule_children, dependent: :destroy
  belongs_to :field
  validates :field_id, presence: true
  validates_attachment_file_name :verification_photo, matches: [/png\Z/, /jpe?g\Z/, /doc?x\Z/, /pdf\Z/]

  def self.default_scope
    where('? = ANY(users.roles)', User.role_types[:doctor])
  end

  def full_name
    title.to_s + ' ' + name + ', ' + field.code
  end

  def upgrade_to_doctor
    roles << User.role_types[:doctor] unless roles.include? User.role_types[:doctor]
  end

  # define searchkick index
  def search_data
    {
      title: title,
      name: name,
      code: field.code,
      field_name: field.name
    }
  end
end

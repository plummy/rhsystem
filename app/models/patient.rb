class Patient < User
  has_many :patient_queues
  has_many :favorites

  def self.default_scope
    where('? = ANY(users.roles)', User.role_types[:patient])
  end

  def upgrade_to_doctor
    roles << User.role_types[:doctor] unless roles.include? User.role_types[:doctor]
  end
end

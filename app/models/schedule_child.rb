class ScheduleChild < ActiveRecord::Base
  belongs_to :schedule
  has_many :patient_queues, dependent: :destroy
  belongs_to :doctor
  belongs_to :location

  enum postponed_for: {
    never: 0,
    fifteen_minutes: 15,
    thirty_minutes: 30,
    one_hour: 60,
    two_hours: 120
  }

  scope :based_on_doctor_and_location, -> (doctor_id, location_id) { where('schedules.doctor_id = ? AND schedules.location_id = ?', doctor_id, location_id) }
  scope :based_on_location, -> (location_id) { where('schedules.location_id = ?', location_id) }
  scope :is_between, -> (start_time, end_time) { where('schedule_children.start_time >= ? AND schedule_children.start_time <= ?', start_time, end_time) }
  scope :starts_on, ->(date) { where('DATE(schedule_children.start_time) = DATE(?)', date).order('schedule_children.start_time') }
  scope :get_daily_schedule_on_a_month, lambda {
    select('DATE(schedule_children.start_time) as start_date')
      .is_between(Time.now, 30.days.from_now)
      .group('DATE(schedule_children.start_time)')
  }
  scope :hasnt_signed_up_by_patient, -> (patient_id) { where('schedule_children.id NOT IN (SELECT pq.schedule_child_id FROM patient_queues pq WHERE pq.patient_id = ?) ', patient_id) }
  scope :doctor_is_not_current_user, -> (doctor_id) { where('schedule_children.doctor_id <> ?', doctor_id) }
  scope :has_space, -> { where('schedule_children.limit > (SELECT count(pq.id) FROM patient_queues pq WHERE schedule_children.id = pq.schedule_child_id)') }
  scope :valid_schedules_for_patient, lambda { |date, patient_id|
    starts_on(date)
      .hasnt_signed_up_by_patient(patient_id)
      .doctor_is_not_current_user(patient_id)
      .has_space
  }

  validates_presence_of :start_time, :end_time, message: 'wrong format/empty'
  validate :start_time_must_be_before_end_time

  def self.time_zone_aware_attributes
    false
  end

  def self.update_all_schedule_children(params, schedule_id)
    start_hour = params[:start_time].split(' ')[1]
    end_hour = params[:end_time].split(' ')[1]
    table = ScheduleChild.arel_table
    update_manager = Arel::UpdateManager.new Arel::Table.engine
    update_manager.table(table)

    cast_start = Arel::Nodes::NamedFunction.new 'DATE', [Arel::Nodes.build_quoted(table[:start_time])]
    cast_end = Arel::Nodes::NamedFunction.new 'DATE', [Arel::Nodes.build_quoted(table[:end_time])]

    concat_start = Arel::Nodes::NamedFunction.new 'CONCAT', [cast_start, Arel::Nodes.build_quoted(' ' + start_hour)]
    concat_end = Arel::Nodes::NamedFunction.new 'CONCAT', [cast_end, Arel::Nodes.build_quoted(' ' + end_hour)]

    timestamp_start = Arel::Nodes::NamedFunction.new 'to_timestamp', [concat_start, Arel::Nodes.build_quoted('YYYY-MM-DD hh24:mi:ss')]
    timestamp_end = Arel::Nodes::NamedFunction.new 'to_timestamp', [concat_end, Arel::Nodes.build_quoted('YYYY-MM-DD hh24:mi:ss')]

    start_sql = Arel::Nodes::SqlLiteral.new timestamp_start.to_sql
    end_sql = Arel::Nodes::SqlLiteral.new timestamp_end.to_sql

    update_manager.set([
                         [table[:start_time], start_sql],
                         [table[:end_time], end_sql],
                         [table[:title], params[:title]],
                         [table[:postponed_for], params[:postponed_for]]]).where(table[:schedule_id].eq(schedule_id))
    ActiveRecord::Base.connection.execute update_manager.to_sql
  end

  def get_start_time
    start_time + ScheduleChild.postponed_fors[postponed_for].minutes
  end

  def get_end_time
    end_time + ScheduleChild.postponed_fors[postponed_for].minutes
  end

  def start_time_must_be_before_end_time
    unless start_time.nil? || end_time.nil?
      errors.add(:end_time, 'must be larger than start time') unless start_time < end_time
    end
  end
end

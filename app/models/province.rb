class Province < ActiveRecord::Base
  # Relations
  has_many :regencies, dependent: :destroy
  has_many :locations, dependent: :destroy
end

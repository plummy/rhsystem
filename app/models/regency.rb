class Regency < ActiveRecord::Base
  validates :name, presence: true
  # Relations
  belongs_to :province
  has_many :locations, dependent: :destroy
end

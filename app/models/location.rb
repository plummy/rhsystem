class Location < ActiveRecord::Base
  # Validation
  validates :name, presence: true
  validates :street, presence: true
  geocoded_by :full_street
  reverse_geocoded_by :latitude, :longitude

  # Relations
  belongs_to  :doctor
  belongs_to  :province
  belongs_to  :regency

  # Geocoder full address
  def full_street
    "#{street}, #{city}, #{province}, #{country}"
  end
end

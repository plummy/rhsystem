module SchedulesHelper
  require 'action_view/helpers/javascript_helper'
  include ActionView::Helpers::JavaScriptHelper

  def display_favorite_button(doctor_id, patient_id)
    favorite = Favorite.new(doctor_id: doctor_id, patient_id: patient_id)
    unless favorite.it_self?
      if favorite.is_exist?
        render 'favorites/favorite_label'
      else
        button_to t('.add_to_favorites'), dashboard_favorites_path(favorite: { doctor_id: doctor_id }), remote: true, id: 'add-to-favorites', class: 'btn btn-primary'
      end
    end
  end
end

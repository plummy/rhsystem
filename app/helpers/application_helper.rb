module ApplicationHelper
  # Returns the full title on a per-page basis
  def full_title(page_title)
    base_title = 'Rhsystem'
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  # Determine sidebar based on user's role
  def active_sidebar
    sidebar = if active_role?(:doctor)
                'layouts/sidebar_doctor'
              else
                'layouts/sidebar_patient'
              end
    request_from_settings_or_users? ? 'layouts/sidebar_settings' : sidebar
  end

  # Check if request comes from settings or users
  def request_from_settings_or_users?
    (splited_request.second == 'dashboard' &&
     splited_request.third == 'settings') ||
      splited_request.second == 'users'
  end

  def splited_request
    request.path.split('/')
  end

  # Gives active class for current page
  def current_page(path)
    'active' if current_page? path
  end

  # Returns link to switch role
  def role_switcher_menu
    if active_role? :doctor
      link_to t('.switch_to_patient'), dashboard_patient_path
    elsif active_role?(:patient) && current_user.is?(:doctor)
      link_to t('.switch_to_doctor'), dashboard_doctor_path
    end
  end

  def path_to_doctor_schedule(doctor)
    if is_dashboard?
      dashboard_search_schedule_path(doctor)
    else
      search_schedule_path(doctor)
    end
  end

  private :splited_request, :request_from_settings_or_users?
end

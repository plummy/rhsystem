module LocationsHelper
  def get_origin_value
    is_from_settings? ? :settings : :normal
  end

  def is_from_settings?
    request.path.split('/').second ==
      'dashboard' &&
      request.path.split('/').third ==
        'settings'
  end
end

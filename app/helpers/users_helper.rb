module UsersHelper
  def path_to_search
    ('/dashboard' if is_dashboard?).to_s + '/search'
  end

  def default_profile_info
    'users/shared/' + active_role.to_s + '_profile' unless active_role.nil?
  end
end

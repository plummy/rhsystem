class ChildEventJob < ActiveJob::Base
  queue_as :childeventqueue

  def perform(schedule)
    @schedule = schedule
    @latest_schedule_child = get_latest_schedule_child(@schedule.id)

    if @latest_schedule_child.nil?
      initial_date = @schedule.start_date
      new_time = initial_date
    elsif !@latest_schedule_child.nil?
      initial_date = @latest_schedule_child.start_time
      new_time = initial_date
    end

    end_time = "#{new_time.strftime('%Y-%m-%d')} #{@schedule.end_time.strftime('%H:%M')}"
    new_time = "#{new_time.strftime('%Y-%m-%d')} #{@schedule.start_time.strftime('%H:%M')}"
    create_schedule_child @schedule.id, new_time, end_time
    new_time = parse_time new_time

    create_child(@schedule.id, initial_date, new_time) unless @schedule.repeat == 'single_event'
  end

  def create_child(id, initial_date, new_time)
    case @schedule.repeat_freq
    when 'daily'
      loops id, initial_date, new_time, 1
    when 'weekly'
      loops id, initial_date, new_time, 7
    when 'monthly'
      loops id, initial_date, new_time, 30
    end
  end

  def loops(id, initial_date, new_time, how_many)
    while ((initial_date - 1.day)..(initial_date + 1.month)).cover?(new_time)
      end_time = "#{(new_time + how_many.days).strftime('%Y-%m-%d')} #{@schedule.end_time.strftime('%H:%M')}"
      new_time = "#{(new_time + how_many.days).strftime('%Y-%m-%d')} #{@schedule.start_time.strftime('%H:%M')}"

      create_schedule_child id, new_time, end_time
      new_time = parse_time new_time
    end
  end

  def parse_time(new_time)
    Time.parse new_time
  end

  def create_schedule_child(id, new_time, end_time)
    ScheduleChild.create!(
      title: @schedule.title,
      limit: @schedule.limit,
      location_id: @schedule.location_id,
      schedule_id: id,
      start_time: new_time,
      end_time: end_time,
      doctor_id: @schedule.doctor_id,
      postponed_for: 0,
      note: @schedule.note,
      limit: @schedule.limit
    )
  end

  def get_schedule(id)
    Schedule.find(id)
  end

  def get_latest_schedule_child(id)
    ScheduleChild.where(schedule_id: id).order(start_time: :desc).first
  end
end

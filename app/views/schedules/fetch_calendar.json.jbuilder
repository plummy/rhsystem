json.array!(@calendar) do |schedule|
  json.extract! schedule, :id, :title
  json.start schedule.get_start_time
  json.end schedule.get_end_time
  json.url show_queue_path(id: schedule.id)
end

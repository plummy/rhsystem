json.array!(@locations) do |location|
  json.extract! location, :id, :street, :city, :province, :country, :doctors_id, :is_active
  json.url location_url(location, format: :json)
end

json.array!(@patient_queues) do |patient_queue|
  json.extract! patient_queue, :id
  json.url patient_queue_url(patient_queue, format: :json)
end

json.array!(@doctor.schedule_children.get_daily_schedule_on_a_month) do |event|
  json.id event.start_date.strftime('%Y-%m-%d')
  json.start event.start_date.strftime('%Y-%m-%d')
end

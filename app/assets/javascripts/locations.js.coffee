# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

initialize = ->
  location_function()

location_function = ->
  regencies = $('#regency_select').html()
  $('#province_select').change ->
    province_id = $(this).val()
    province = $('#province_select :selected').text()
    console.log province
    options = $(regencies).filter("optgroup[label='#{province}']").html()
    if options
      $('#regency_select').html(options)
    else
      $('#regency_select').empty()
 
 $(document).ready(initialize)
 $(document).on('page:load', initialize)
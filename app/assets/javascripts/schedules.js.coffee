# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

initialize = ->
  schedule_function()

schedule_function = ->
  $(document).on "click", "a[data-href]", ->
    $.ajax(
      dataType: "script"
      url: $(this).data("href") #equivalent to $(this).attr("data-href")
      context: $("a.status_link")
      success: (data) ->
      	jsonData = data
      	
    )

  $(".delete_schedule").bind "ajax:success", ->
    $(this).closest('tr').fadeOut "slow", ->
      $("table tr.numbered_row:visible").each (i) -> 
        $(this).children(".seq").text i + 1

  $.rails.allowAction = (link) ->
    return true unless link.attr('data-confirm')
    $.rails.showConfirmDialog(link) # look bellow for implementations
    false # always stops the action since code runs asynchronously
   
  $.rails.confirmed = (link) ->
    link.removeAttr('data-confirm')
    link.trigger('click.rails')


  $.rails.showConfirmDialog = (link) ->
    message = link.attr 'data-confirm'
    
    html = """
            <div class="modal" id="confirmationDialog" top="10px">
              <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                      #{message}
                    </div>
                    <div class="modal-footer">
                      <a data-dismiss="modal" class="btn">Cancel</a>
              """
    $calendarElement = $("#calendar")
    if $calendarElement.length                        
      html += """
              <a data-dismiss="modal" class="btn btn-default delete_single">Delete This Schedule Only</a>
              <a data-dismiss="modal" class="btn btn-danger confirm">Delete All Future Schedules</a>
                    
            """
    else
      html += """
              <a data-dismiss="modal" class="btn btn-danger confirm">Delete</a>
                    
            """
    html += """
                      </div>
                    </div>
                  </div>
                </div>
            """
    $(html).modal()
    $('#confirmationDialog .confirm').on 'click', -> $.rails.confirmed(link)
    $('#confirmationDialog .delete_single').on 'click', -> 
      window.location.origin = setWindowUrl()
      href = link.attr("href")
      id = href.split("/")[3]
      link.attr("href", window.location.origin + "/schedule_children/destroy_one/" + id)
      $.rails.confirmed(link)
    


  isRecurringEvent = (id) ->
    eventParam = {
      id: id
    }
    window.location.origin = setWindowUrl()

    $.ajax(
      dataType: "json"
      url: window.location.origin + "/schedule_children/is_recurring"
      success: (data) ->
        jsonData = data
        
        if data == false
          return false
        else
          return true
      data: eventParam
    )



  $body = $("body")
  $(document).on
    ajaxStart: ->
      $body.addClass "loading"
      return
    ajaxStop: ->
      $body.removeClass "loading"
      return

  setTimeout (->
    renderCalendarAndTable()
    return
  ), 10000

  setInterval (->
    renderCalendarAndTable()
    return
  ), 600000


  setWindowUrl = ->
    if not window.location.origin
      return window.location.protocol+"//"+window.location.host

  renderCalendarAndTable = ->
    $calendarElement = $("#calendar")
    if $calendarElement.length
      $("#calendar").fullCalendar "refetchEvents"
      scheduleInfo = {
        page: 1,
      }
      window.location.origin = setWindowUrl()
      $.ajax(
        dataType: "script"
        url: window.location.origin + "/schedules/fetch_pagination"
        contentType: "application/javascript"
        success: (data) ->
          ## do nothing  
        data: scheduleInfo
      )

  changeSchedule = (id, start, end) ->
    calendarParams = {
      id: id,
      start_time: start,
      end_time: end
    }
    
    window.location.origin = setWindowUrl()

    $.ajax(
      dataType: "script"
      url: window.location.origin + "/schedules/change_schedule"
      contentType: "application/javascript"
      success: (data) ->
        ## do nothing
      error: (data) ->
        alert("Failed")
        
      data: calendarParams
    )

  $(document).ready ->
    date = new Date()
    date.setHours(0,0,0,0)

    d = date.getDate()
    m = date.getMonth()
    y = date.getFullYear()

    window.location.origin = setWindowUrl()

    $("#calendar").fullCalendar
      theme: true
      header:
        left: "prev,next today"
        center: "title"
        right: "month,agendaWeek,agendaDay"
      editable: true,
      
      # add event name to title attribute on mouseover
      eventMouseover: (event, jsEvent, view) ->
        $(jsEvent.target).attr "title", event.title  if view.name isnt "agendaDay"
        return

      # reschedule behaviour
      eventResize: (event, delta, revertFunc) ->
        if not confirm "Are you sure about this change?"
          revertFunc()
        else
          if event.start < date
            alert("You can't choose a date that already past.")
            revertFunc()
          else
            changeSchedule(event.id, event.start.format(), event.end.format()  )

      eventDrop: (event, delta, revertFunc) ->
        if not confirm "Are you sure about this change?"
          revertFunc()
        else
          if event.start < date
            alert("You can't choose a date that already past.")
            revertFunc()
          else
            changeSchedule(event.id, event.start.format(), event.end.format()  )
            

      eventClick: (event) ->
        if event.start < date
          alert("You can't choose a date that already past.")
          return false
        else
          window.open event.url, event.title, "height=640,width=480"
          return false

      # Event sources
      events: window.location.origin + "/schedules/fetch_calendar.json"

    return




      
    
$(document).ready(initialize)
$(document).on('page:load', initialize)
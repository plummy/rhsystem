namespace :db do
  desc 'Truncate all tables'
  task truncate: :environment do
    conn = ActiveRecord::Base.connection
    tables = conn.execute("select tablename from pg_tables where schemaname='public' and tablename <> 'schema_migrations'").map { |r| r['tablename'] }
    tables.delete 'schema_migrations'
    tables.each { |t| conn.execute("TRUNCATE #{t} RESTART IDENTITY CASCADE") }
  end
end

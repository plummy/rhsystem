namespace :db do
  desc 'fill the database'
  task populate: :environment do
    puts 'Adding a doctor and referenced objects'

    20.times do |_d|
      name = Faker::Name.name
      field = Field.offset(rand(Field.count)).first
      doctor = User.create!(
        name: name,
        address: Faker::Address.street_address,
        phone: Faker::PhoneNumber.phone_number,
        password: 'password',
        email:  name.split.join.downcase + '@example.com',
        field_id: field.id,
        roles:  [0, 1],
        description: Faker::Lorem.paragraph(2),
        title: ['prof.', 'dr.', 'Dr.'].sample,
        avatar: Faker::Avatar.image('my-own-slug')
      )
      regency = Regency.offset(rand(Regency.count)).first
      5.times do |_l|
        Location.create!(
          name: Faker::Company.name,
          street: Faker::Address.street_address,
          province_id: regency.province_id,
          phone: Faker::PhoneNumber.phone_number,
          regency_id: regency.id,
          country: 'ID',
          latitude: Faker::Address.latitude,
          longitude: Faker::Address.longitude,
          doctor_id: doctor.id
        )
      end
    end

    puts 'creating schedule for each locations'
    locations = Location.all
    locations.each do |location|
      10.times do |_s|
        time = Faker::Time.between(1.day.ago,
                                   Time.zone.now + 6.days,
                                   :afternoon)
        repeat = Schedule.repeats.values.sample
        repeat_freq = repeat == 0 ? 0 : Schedule.repeat_freqs.values.sample
        schedule = Schedule.create!(
          days_id: rand(1..7),
          start_time: time,
          end_time: time + 2.hours,
          doctor_id: location.doctor_id,
          location_id: location.id,
          repeat: repeat,
          repeat_freq: repeat_freq,
          limit: rand(9..100),
          start_date: Faker::Date.between(1.day.ago, Time.zone.today + 7.days),
          title: Faker::Lorem.word,
          note: Faker::Lorem.paragraph(2)
        )
        ChildEventJob.perform_now schedule
      end
    end

    puts 'Adding patient and related objects'

    10.times do |_p|
      name = Faker::Name.name
      Patient.create!(
        name: name,
        address: Faker::Address.street_address,
        phone: Faker::PhoneNumber.phone_number,
        password: 'password',
        email:  name.split.join.downcase + '@example.com'
      )
    end

    puts 'Creating patient queue for each patient'

    patients = Patient.all
    patients.each do |patient|
      100.times do |_pq|
        schedule_child = ScheduleChild.select(:id)
                                      .offset(rand(ScheduleChild.count))
                                      .first
        PatientQueue.create!(
          patient_id: patient.id,
          schedule_child_id: schedule_child.id
        )
      end
    end
  end
end

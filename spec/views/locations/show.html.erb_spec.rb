require 'spec_helper'

describe 'locations/show' do
  before(:each) do
    @location = assign(:location, stub_model(Location,
                                             street: 'Street',
                                             city: 'City',
                                             province: 'Province',
                                             country: 'Country',
                                             doctors_id: '',
                                             is_active: ''
                                            ))
  end

  xit 'renders attributes in <p>' do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Street/)
    rendered.should match(/City/)
    rendered.should match(/Province/)
    rendered.should match(/Country/)
    rendered.should match(//)
    rendered.should match(//)
  end
end

require 'spec_helper'

describe 'locations/edit' do
  before(:each) do
    @location = assign(:location, stub_model(Location,
                                             street: 'MyString',
                                             city: 'MyString',
                                             province: 'MyString',
                                             country: 'MyString',
                                             doctors_id: '',
                                             is_active: ''
                                            ))
  end

  xit 'renders the edit location form' do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select 'form', action: locations_path(@location), method: 'post' do
      assert_select 'input#location_street', name: 'location[street]'
      assert_select 'input#location_city', name: 'location[city]'
      assert_select 'input#location_province', name: 'location[province]'
      assert_select 'input#location_country', name: 'location[country]'
      assert_select 'input#location_doctors_id', name: 'location[doctors_id]'
      assert_select 'input#location_is_active', name: 'location[is_active]'
    end
  end
end

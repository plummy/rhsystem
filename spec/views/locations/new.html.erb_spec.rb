require 'spec_helper'

describe 'locations/new' do
  before(:each) do
    assign(:location, stub_model(Location,
                                 street: 'MyString',
                                 city: 'MyString',
                                 province: 'MyString',
                                 country: 'MyString',
                                 doctors_id: '',
                                 is_active: ''
                                ).as_new_record)
  end

  xit 'renders new location form' do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select 'form', action: locations_path, method: 'post' do
      assert_select 'input#location_street', name: 'location[street]'
      assert_select 'input#location_city', name: 'location[city]'
      assert_select 'input#location_province', name: 'location[province]'
      assert_select 'input#location_country', name: 'location[country]'
      assert_select 'input#location_doctors_id', name: 'location[doctors_id]'
      assert_select 'input#location_is_active', name: 'location[is_active]'
    end
  end
end

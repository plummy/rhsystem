require 'spec_helper'

describe 'locations/index' do
  before(:each) do
    assign(:locations, [
             stub_model(Location,
                        street: 'Street',
                        city: 'City',
                        province: 'Province',
                        country: 'Country',
                        doctors_id: '',
                        is_active: ''
                       ),
             stub_model(Location,
                        street: 'Street',
                        city: 'City',
                        province: 'Province',
                        country: 'Country',
                        doctors_id: '',
                        is_active: ''
                       )
           ])
  end

  xit 'renders a list of locations' do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select 'tr>td', text: 'Street'.to_s, count: 2
    assert_select 'tr>td', text: 'City'.to_s, count: 2
    assert_select 'tr>td', text: 'Province'.to_s, count: 2
    assert_select 'tr>td', text: 'Country'.to_s, count: 2
    assert_select 'tr>td', text: ''.to_s, count: 2
    assert_select 'tr>td', text: ''.to_s, count: 2
  end
end

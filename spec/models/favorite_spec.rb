require 'spec_helper'

describe Favorite, type: :model do
  it { is_expected.to validate_presence_of :doctor }
  it { is_expected.to validate_presence_of :patient }
  it { is_expected.to belong_to :doctor }
  it { is_expected.to belong_to :patient }
  it { expect(FactoryGirl.build(:favorite)).to be_valid }
  it { expect(Favorite.create).to_not be_valid }
  it { expect(Favorite.create(doctor_id: 1, patient_id: 1)).to_not be_valid }

  it 'is not yet exist' do
    FactoryGirl.create(:favorite)
    expect(FactoryGirl.create(:favorite)).to_not be_valid
  end
end

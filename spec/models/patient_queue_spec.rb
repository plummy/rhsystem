require 'spec_helper'

describe PatientQueue, type: :model do
  it { is_expected.to validate_presence_of :schedule_child }
  it { is_expected.to validate_presence_of :patient }
  it { is_expected.to belong_to :schedule_child }
  it { is_expected.to belong_to :patient }
  it { expect(FactoryGirl.build(:patient_queue)).to be_valid }
  it { expect(PatientQueue.create).to_not be_valid }

  it 'is not yet exist' do
    FactoryGirl.create(:patient_queue)
    expect(FactoryGirl.create(:patient_queue)).to_not be_valid
  end
end

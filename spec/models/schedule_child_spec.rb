require 'spec_helper'

describe ScheduleChild do
  it 'has a endtime that larger than starttime' do
    expect(FactoryGirl.build(:schedule_child, start_time: '01-01-2014 04:00', end_time: '01-01-2014 02:00')).to_not be_valid
  end

  it 'displays correct start time and end time' do
    start_time = '01-01-2014 04:00'
    end_time = '01-01-2014 06:00'
    schedule = FactoryGirl.create(:schedule_child, start_time: start_time, end_time: end_time, postponed_for: :never)
    expect(schedule.get_start_time).to eq '01-01-2014 04:00'
    expect(schedule.get_end_time).to eq '01-01-2014 06:00'
  end

  it 'has valid start time' do
    expect(FactoryGirl.build(:schedule_child, start_time: 'abc')).to_not be_valid
  end

  it 'has valid end time' do
    expect(FactoryGirl.build(:schedule_child, end_time: 'abc')).to_not be_valid
  end
end

require 'spec_helper'

describe Schedule do
  xit 'has a endtime that larger than starttime' do
    expect(FactoryGirl.build(:schedule, start_time: '01-01-2014 04:00', end_time: '01-01-2014 02:00')).to_not be_valid
  end
  xit "has correct days's id" do
    schedule = FactoryGirl.create(:schedule, start_date: '01-01-2015 04:00')
    expect(schedule.days_id).to eq 'thursday'
  end
  xit 'returns start date as time' do
    schedule = FactoryGirl.build(:schedule, start_date: '01-01-2015 04:00')
    expect(schedule.start_date).to be_a Time
  end

  xit 'sets correct repeat' do
    schedule = FactoryGirl.create(:schedule, repeat_freq: :daily)
    expect(schedule.repeat).to eq 'recurring_event'
    schedule = FactoryGirl.create(:schedule, repeat_freq: :weekly)
    expect(schedule.repeat).to eq 'recurring_event'
    schedule = FactoryGirl.create(:schedule, repeat_freq: :monthly)
    expect(schedule.repeat).to eq 'recurring_event'
    schedule = FactoryGirl.create(:schedule, repeat_freq: :never)
    expect(schedule.repeat).to eq 'single_event'
  end
end

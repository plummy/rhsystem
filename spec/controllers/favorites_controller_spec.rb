require 'spec_helper'

describe FavoritesController, type: :controller do
  login_patient

  def valid_attributes
    doctor = FactoryGirl.create(:doctor)
    FactoryGirl.attributes_for(:favorite, doctor_id: doctor.id, patient_id: @patient.id)
  end

  describe 'GET index' do
    before { get :index }

    it { expect(response).to render_template(:index) }
    it { expect(response).to be_success }
    it { expect(response).to have_http_status(200) }

    it 'assigns all favorites as @favorites' do
      favorite = FactoryGirl.create(:favorite, patient_id: @patient.id)
      expect(assigns(:favorites)).to eq([favorite])
    end

    it 'blocks unauthenticated access', :skip_before do
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  describe 'POST create' do
    context 'with valid params' do
      let(:post_favorite) { post :create, favorite: valid_attributes, format: :js }

      it { expect { post_favorite }.to change(Favorite, :count).by(1) }
      it { expect(post_favorite).to render_template :create }

      it 'assigns a newly created location as @favorite' do
        post_favorite
        favorite = assigns(:favorite)

        expect(favorite).to be_a(Favorite)
        expect(favorite).to be_persisted
      end
    end
  end
end

require 'spec_helper'

describe PatientQueuesController, type: :controller do
  login_patient

  describe 'POST create' do
    let(:schedule_child) { FactoryGirl.create(:schedule_child) }
    let(:post_queue) { post :create, schedule_child_id: schedule_child.id, format: :js }

    it { expect { post_queue }.to change(PatientQueue, :count).by(1) }
    it { expect(post_queue).to render_template :create }

    it 'assigns a newly created patient_queue as @patient_queue' do
      post_queue
      queue = assigns(:patient_queue)

      expect(queue).to be_a(PatientQueue)
      expect(queue).to be_persisted
    end
  end

  describe 'GET appointments' do
    before { get :appointments }

    it { expect(response).to render_template('patient_queues/appointments/index') }
    it { expect(response).to be_success }
    it { expect(response).to have_http_status(200) }

    it 'assigns all appointments as @appointments' do
      appointment = FactoryGirl.create(:patient_queue, patient_id: @patient.id)
      expect(assigns(:appointments)).to eq([appointment])
    end

    it 'blocks unauthenticated access', :skip_before do
      expect(response).to redirect_to(root_path)
    end
  end
end

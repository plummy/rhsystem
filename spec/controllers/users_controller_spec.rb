require 'spec_helper'

describe UsersController, type: :controller do
  login_patient

  describe 'GET search' do
    context 'index page' do
      let(:get_index) { get :search }

      it { expect(get_index).to render_template('users/search/index') }
      it { expect(get_index).to be_success }
      it { expect(get_index).to have_http_status(200) }

      it 'allows unauthenticated access', :skip_before do
        @request.env['devise.mapping'] = Devise.mappings[:user] # Try to remove it why it is here
        expect(get_index).to be_success
      end
    end

    context 'with keyword' do
      let!(:doctor) { FactoryGirl.create(:doctor) }

      before(:each) do
        Doctor.reindex
        xhr :get, :search, keyword: doctor.name, format: :js
        # Your curiosity is over. Check here why 'xhr' https://github.com/rspec/rspec-rails/issues/950
      end

      it { expect(response).to render_template('users/search/results') }
      it { expect(assigns(:doctors).to_a).to eq([doctor]) }
    end
  end
end

require 'spec_helper'

describe FavoritesController, type: :controller do
  login_patient

  describe 'routing' do
    it { expect(get: dashboard_favorites_path).to route_to('favorites#index') }
    it { expect(post: dashboard_favorites_path).to route_to('favorites#create') }
  end
end

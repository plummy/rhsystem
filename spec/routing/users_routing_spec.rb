require 'spec_helper'

describe UsersController, type: :controller do
  login_patient

  describe 'routing' do
    it { expect(get: search_path).to route_to('users#search') }
    it { expect(get: '/search/schedule/1').to route_to('schedule_children#doctor', id: '1') }
    it { expect(get: dashboard_search_path).to route_to('users#search') }
    it { expect(get: '/dashboard/search/schedule/1').to route_to('schedule_children#doctor', id: '1') }
  end
end

require 'spec_helper'

describe PatientQueuesController, type: :controller do
  login_patient

  describe 'routing' do
    it { expect(get: dashboard_appointments_path).to route_to('patient_queues#appointments') }
    it { expect(post: dashboard_appointments_path).to route_to('patient_queues#create') }
    it { expect(get: '/dashboard/appointments/1').to route_to('patient_queues#show', id: '1') }
  end
end

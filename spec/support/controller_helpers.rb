module ControllerHelpers
  def login_doctor
    before :each do |example|
      unless example.metadata[:skip_before]
        @request.env['devise.mapping'] = Devise.mappings[:user]
        @doctor = FactoryGirl.create(:doctor)
        sign_in :user, @doctor
      end
    end
  end

  def login_patient
    before :each do |example|
      unless example.metadata[:skip_before]
        @request.env['devise.mapping'] = Devise.mappings[:user]
        @patient = FactoryGirl.create(:patient)
        sign_in :user, @patient
      end
    end
  end
end

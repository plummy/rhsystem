require 'faker'

FactoryGirl.define do
  factory :schedule_child do |f|
    time = Faker::Time.between(1.day.ago, Time.now + 2.days, :afternoon)
    postponed_for = ScheduleChild.postponed_fors.values.sample

    f.start_time { time }
    f.end_time { time + 2.hours }
    f.postponed_for { postponed_for }
    f.title { Faker::Lorem.word }
  end
end

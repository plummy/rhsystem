require 'faker'

FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "full_name_#{n}" }
    address 'valid address'
    phone '1234567890'
    password 'password'
    sequence(:email) { |n| "valid_email_#{n}@example.com" }
  end

  factory :doctor, class: Doctor, parent: :user do
    roles [0, 1]
    field
  end

  factory :patient, class: Patient, parent: :user do
    roles [1]
  end
end

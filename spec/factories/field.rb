require 'faker'

FactoryGirl.define do
  factory :field do |f|
    f.name 'Spesialis Anak'
    f.code 'Sp.A'
  end
end

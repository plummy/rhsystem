require 'faker'

FactoryGirl.define do
  factory :schedule do |f|
    time = Faker::Time.between(1.day.ago, Time.now + 2.days, :afternoon)
    repeat_freq = Schedule.repeat_freqs.values.sample

    f.start_time { time }
    f.end_time { time + 2.hours }
    f.repeat_freq { repeat_freq }
    f.start_date { Faker::Date.between(1.day.ago, Date.today + 7.days) }
    f.title { Faker::Lorem.word }
  end
end

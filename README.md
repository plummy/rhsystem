# README #

This README would normally document whatever steps are necessary to get your application up and running.


### Initial Setup ###

This is a one-time setup only.

1. use Ruby version >= 2.x.x
2. Install redis
3. install [elasticsearch](http://www.elasticsearch.org/) and run it
4. install postgresql without password and run it
5. clone this project
6. run `bundle install` in this project root folder
7. setup migration `rake db:create db:migrate`
8. run resque `QUEUE=* rake environment resque:work`
9. populate data `rake db:seed db:populate` *Note : populating the data will take a long time, prepare a coffee. However you can adjust the number of data generated in populate.rake*
10. reindex searchkick. execute `Doctor.reindex` on `rails c`

### How to run it ###

1. start redis server by `redis-server`
2. start elasticsearch service
3. start postgresql
4. do `git pull`
5. you might required to run `bundle install` if there is a new gem
6. you might required to run `rake db:migration` if there is a new migration
7. do `rake db:reset` if there is a new seed data
8. run Rails server `rails s`

### To reset the data ###

`rake db:truncate`

### Contribution guidelines ###

* Pull this project
* Create a branch on your local
* Do your part
* Make sure everything is working
* And do our [git flow](http://reinh.com/blog/2009/03/02/a-git-workflow-for-agile-teams.html)

### Got a bug or need something to improve? ###
* `!it_is_a_question?` Use [Issues](https://bitbucket.org/plummy/rhsystem/issues?status=new&status=open) : Use Hangouts
* Use [Trello](https://trello.com) `if it_is_a_concept?`

### Need someone you can talk to? ###
You know where I am.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact